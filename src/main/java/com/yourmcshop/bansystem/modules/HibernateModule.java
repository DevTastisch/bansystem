package com.yourmcshop.bansystem.modules;

import  com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.provider.SessionFactoryProvider;
import org.hibernate.SessionFactory;

public class HibernateModule extends AbstractModule {

    protected void configure() {
        this.bindConstant().annotatedWith(Names.named("databaseUrl")).to("jdbc:mysql://" + BanSystem.getInstance().getMainConfig().getString("mysql.host") + "/" + BanSystem.getInstance().getMainConfig().getString("mysql.database"));
        this.bindConstant().annotatedWith(Names.named("databaseUser")).to(BanSystem.getInstance().getMainConfig().getString("mysql.user"));
        this.bindConstant().annotatedWith(Names.named("databasePassword")).to(BanSystem.getInstance().getMainConfig().getString("mysql.password"));
        this.bind(SessionFactory.class).toProvider(SessionFactoryProvider.class).asEagerSingleton();
    }

}
