package com.yourmcshop.bansystem;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.yourmcshop.bansystem.config.YamlConfig;
import com.yourmcshop.bansystem.controller.MessageController;
import com.yourmcshop.bansystem.controller.PunishController;
import com.yourmcshop.bansystem.modules.HibernateModule;
import com.yourmcshop.bansystem.repository.repositories.PunishEntryRepository;
import com.yourmcshop.bansystem.repository.repositories.PunishUserRepository;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class BanSystem extends Plugin {

    private static BanSystem instance;
    private PunishController punishController;
    private MessageController messageController;
    private PunishUserRepository punishUserRepository;
    private PunishEntryRepository punishEntryRepository;
    private YamlConfig mainConfig;
    private Injector injector;


    public void onEnable() {
        instance = this;
        this.initControllers();
        this.mainConfig = YamlConfig.of(String.format("./plugins/%s/config.yml", this.getDescription().getName()));
        this.injector = Guice.createInjector(new HibernateModule());
        this.initRepositories();
        this.initListeners();
        this.initCommands();
    }

    private void initControllers() {
        this.punishController = new PunishController(this);
        this.messageController = new MessageController(this);
    }

    private void initRepositories() {
        this.punishUserRepository = new PunishUserRepository();
        this.punishEntryRepository = new PunishEntryRepository();
    }

    private void initListeners() {
        new Reflections("com.yourmcshop.bansystem", this.getClass().getClassLoader(), new SubTypesScanner(false)).getSubTypesOf(Listener.class)
                .forEach(listenerClass -> {
                    try {
                        this.getProxy().getPluginManager().registerListener(this, listenerClass.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void initCommands() {
        new Reflections("com.yourmcshop.bansystem", this.getClass().getClassLoader(), new SubTypesScanner(false)).getSubTypesOf(Command.class)
                .forEach(commandClass -> {
                    try {
                        this.getProxy().getPluginManager().registerCommand(this, commandClass.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });
    }

    public Injector getInjector() {
        return injector;
    }

    public PunishController getPunishController() {
        return punishController;
    }

    public MessageController getMessageController() {
        return messageController;
    }

    public YamlConfig getMainConfig() {
        return mainConfig;
    }

    public PunishUserRepository getPunishUserRepository() {
        return punishUserRepository;
    }

    public PunishEntryRepository getPunishEntryRepository() {
        return punishEntryRepository;
    }

    public static BanSystem getInstance() {
        return instance;
    }
}
