package com.yourmcshop.bansystem.config;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class YamlConfig {

    private final Configuration yamlConfiguration;

    private YamlConfig(File file) {
        Configuration load = null;
        try {
            load = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.yamlConfiguration = load;
    }

    private YamlConfig(Configuration yamlConfiguration) {
        this.yamlConfiguration = yamlConfiguration;
    }

    public String getString(String key) {
        return this.yamlConfiguration.getString(key);
    }

    public int getInt(String key) {
        return this.yamlConfiguration.getInt(key);
    }

    public boolean getBoolean(String key) {
        return this.yamlConfiguration.getBoolean(key);
    }

    public List<String> getStringList(String key) {
        return this.yamlConfiguration.getStringList(key);
    }

    public Class getType(String key){
        return this.yamlConfiguration.get(key).getClass();
    }

    public long getLong(String key) {
        return this.yamlConfiguration.getLong(key);
    }

    public YamlConfig getSection(String key) {
        return new YamlConfig(this.yamlConfiguration.getSection(key));
    }

    public Collection<String> getKeys(String key) {
        return this.yamlConfiguration.getSection(key).getKeys();
    }

    public Collection<String> getKeys() {
        return this.yamlConfiguration.getKeys();
    }

    public static YamlConfig of(String path) {
        return new YamlConfig(new File(path));
    }

}
