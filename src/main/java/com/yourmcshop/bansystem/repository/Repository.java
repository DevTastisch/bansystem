package com.yourmcshop.bansystem.repository;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.yourmcshop.bansystem.BanSystem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

public class Repository<T> {

    private final ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    private final SessionFactory sessionFactory;

    public Repository() {
        this.sessionFactory = BanSystem.getInstance().getInjector().getInstance(SessionFactory.class);
    }

    public void save(T t) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.saveOrUpdate(t);
                    transaction.commit();
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void save(T t, Consumer<T> consumer) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.saveOrUpdate(t);
                    transaction.commit();
                    consumer.accept(t);
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void save(T t, Runnable runnable) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.saveOrUpdate(t);
                    transaction.commit();
                    runnable.run();
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


    public void delete(T t) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.delete(t);
                    transaction.commit();
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void delete(T t, Runnable runnable) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.delete(t);
                    transaction.commit();
                    runnable.run();
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void delete(T t, Consumer<T> consumer) {
        try {
            this.getListeningExecutorService().submit(() -> {
                try (Session session = this.getSessionFactory().openSession()) {
                    Transaction transaction = session.beginTransaction();
                    session.delete(t);
                    transaction.commit();
                    consumer.accept(t);
                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public ListeningExecutorService getListeningExecutorService() {
        return listeningExecutorService;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
