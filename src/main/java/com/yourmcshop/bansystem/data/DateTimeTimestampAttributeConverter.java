package com.yourmcshop.bansystem.data;

import com.yourmcshop.bansystem.model.DateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Converter
public class DateTimeTimestampAttributeConverter implements AttributeConverter<DateTime, Timestamp> {

    public Timestamp convertToDatabaseColumn(DateTime attribute) {
        return attribute == null ? null : Timestamp.from(OffsetDateTime.ofInstant(Instant.ofEpochMilli(attribute.getMillis()), ZoneId.of("Europe/Berlin")).toInstant());
    }

    public DateTime convertToEntityAttribute(Timestamp dbData) {
        return dbData == null ? new DateTime(-1) : new DateTime(dbData.getTime());
    }
}
