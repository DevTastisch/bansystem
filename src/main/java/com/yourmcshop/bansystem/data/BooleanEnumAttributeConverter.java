package com.yourmcshop.bansystem.data;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by DevTastisch on 03.03.2019
 */
@Converter
public class BooleanEnumAttributeConverter implements AttributeConverter<Boolean, String> {
    @Override
    public String convertToDatabaseColumn(Boolean aBoolean) {
        return String.valueOf(aBoolean);
    }

    @Override
    public Boolean convertToEntityAttribute(String s) {
        return Boolean.valueOf(s);
    }
}
