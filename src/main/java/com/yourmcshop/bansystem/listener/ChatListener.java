package com.yourmcshop.bansystem.listener;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent event) {
        if (!event.getMessage().startsWith("/") && event.getSender() instanceof ProxiedPlayer) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(((ProxiedPlayer) event.getSender()).getUniqueId()).get();
                if (punishUser.isMuted()) {
                    event.setCancelled(true);
                    if (punishUser.getActiveMute().isInfinity()) {
                        ((ProxiedPlayer) event.getSender()).sendMessage(MessageType.INFO_MUTE_SCREEN_PERMANENT.format(
                                punishUser.getActiveMute().getReasonId() == -1 ? punishUser.getActiveMute().getCustomReason() : punishUser.getActiveMute().getReason().getName()
                        ));
                    } else {
                        ((ProxiedPlayer) event.getSender()).sendMessage(MessageType.INFO_MUTE_SCREEN.format(
                                punishUser.getActiveMute().getRemaining().formatAsTimeUnits(),
                                punishUser.getActiveMute().getReasonId() == -1 ? punishUser.getActiveMute().getCustomReason() : punishUser.getActiveMute().getReason().getName()
                        ));
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

}
