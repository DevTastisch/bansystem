package com.yourmcshop.bansystem.listener;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class LoginListener implements Listener {

    @EventHandler
    public void onJoin(LoginEvent event) {
        try {
            PunishUser banUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(event.getConnection().getUniqueId()).get();
            if (banUser == null) {
                PunishUser newBanUser = new PunishUser(
                        null,
                        event.getConnection().getUniqueId(),
                        event.getConnection().getName(),
                        event.getConnection().getAddress().getHostName(),
                        new ArrayList<>());
                BanSystem.getInstance().getPunishUserRepository().save(newBanUser);
            } else {
                banUser.setLastIp(event.getConnection().getAddress().getHostName());
                BanSystem.getInstance().getPunishUserRepository().save(banUser);
                if (banUser.isBanned()) {
                    event.setCancelled(true);
                    PunishEntry activeBan = banUser.getActiveBan();

                    if (activeBan.isInfinity()) {
                        event.setCancelReason(MessageType.INFO_BAN_SCREEN_PERMANENT.format(
                                activeBan
                        ));
                    } else {
                        event.setCancelReason(MessageType.INFO_BAN_SCREEN.format(
                                activeBan
                        ));
                    }
                } else {
                    List<PunishEntry> punishEntries = BanSystem.getInstance().getPunishEntryRepository().findActiveByIpAndType(event.getConnection().getAddress().getHostName(), PunishType.BAN).get();
                    if (!punishEntries.isEmpty()) {
                        BanSystem.getInstance().getPunishController().punish(
                                ProxyServer.getInstance().getConsole(),
                                event.getConnection().getUniqueId(),
                                BanSystem.getInstance().getPunishController().getAutoIpBanFormat(),
                                event.getConnection().getAddress().getHostName()
                        );
                        PunishEntry entry = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(event.getConnection().getUniqueId()).get().getActiveBan();
                        if (entry.getPunishType() == PunishType.BAN) {
                            if (entry.isInfinity()) {
                                event.getConnection().disconnect(new TextComponent(MessageType.INFO_BAN_SCREEN_PERMANENT.format(
                                        entry
                                )));
                            } else {
                                event.getConnection().disconnect(MessageType.INFO_BAN_SCREEN.format(
                                        entry
                                ));
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
