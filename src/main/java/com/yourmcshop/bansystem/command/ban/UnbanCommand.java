package com.yourmcshop.bansystem.command.ban;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 02.03.2019
 */
public class UnbanCommand extends Command {


    public UnbanCommand() {
        super("unban");
    }

    public void execute(CommandSender commandSender, String[] args) {
        if(!commandSender.hasPermission("bansystem.unban")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(commandSender, args[0]);
                    return;
                }

                PunishEntry punishEntry = punishUser.isBanned() ? punishUser.getActiveBan() : null;

                if (BanSystem.getInstance().getPunishController().unpunish(punishUser.getUuid(), PunishType.BAN)) {
                    ProxyServer.getInstance().getPlayers().stream()
                            .filter(player -> player.hasPermission("bansystem.notify"))
                            .forEach(player -> MessageType.INFO_BROADCAST_UNBAN.sendTo(player,
                                    punishUser,
                                    commandSender instanceof ProxiedPlayer ? commandSender.getName() : MessageType.FORMAT_CONSOLE_BAN
                            ));
                    MessageType.SUCCESS_COMMAND_UNBAN.sendTo(commandSender, punishUser, punishEntry);
                } else {
                    MessageType.ERROR_PLAYER_NOT_BANNED.sendTo(commandSender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        MessageType.COMMAND_SYNTAX_UNBAN.sendTo(commandSender);
    }
}
