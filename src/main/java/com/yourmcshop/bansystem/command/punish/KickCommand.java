package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 23.03.2019
 */
public class KickCommand extends Command {

    public KickCommand() {
        super("kick");
    }

    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("bansystem.kick")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 2) {
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(args[0]);
            if (proxiedPlayer != null) {
                try {
                    MessageType.SUCCESS_COMMAND_KICK.sendTo(sender, BanSystem.getInstance().getPunishUserRepository().findByUniqueID(proxiedPlayer.getUniqueId()).get(), args[0]);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                ProxyServer.getInstance().getPlayers().stream()
                        .filter(player -> player.hasPermission("bansystem.notify"))
                        .forEach(player -> {
                            try {
                                MessageType.INFO_BROADCAST_KICK.sendTo(player,
                                        BanSystem.getInstance().getPunishUserRepository().findByUniqueID(proxiedPlayer.getUniqueId()).get(),
                                        sender instanceof ProxiedPlayer ? sender.getName() : MessageType.FORMAT_CONSOLE_BAN,
                                        args[1]
                                );
                            } catch (InterruptedException | ExecutionException e) {
                                e.printStackTrace();
                            }
                        });
                proxiedPlayer.disconnect(MessageType.INFO_KICK_SCREEN.format(args[1]));
            }
        }
        MessageType.COMMAND_SYNTAX_KICK.sendTo(sender);
    }
}
