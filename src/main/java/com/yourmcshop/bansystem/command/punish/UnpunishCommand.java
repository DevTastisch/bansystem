package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 18.03.2019
 */
public class UnpunishCommand extends Command {

    public UnpunishCommand() {
        super("unpunish");
    }

    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("bansystem.unpunish")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishEntry punishEntry = BanSystem.getInstance().getPunishEntryRepository().findById(args[0], false).get();
                if (punishEntry != null) {
                    BanSystem.getInstance().getPunishEntryRepository().delete(punishEntry);
                    MessageType.SUCCESS_COMMAND_UNPUNISH.sendTo(sender, punishEntry.getUser(), punishEntry);
                    return;
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_UNPUNISH.sendTo(sender);
    }

}
