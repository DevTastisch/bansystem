package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class RemoveIpCommand extends Command {

    public RemoveIpCommand() {
        super("removeip");
    }

    public void execute(CommandSender sender, String[] args) {
        if(!sender.hasPermission("bansystem.removeip")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if(args.length == 1){
            try {
                PunishEntry punishEntry = BanSystem.getInstance().getPunishEntryRepository().findById(args[0], false).get();
                if(punishEntry != null){
                    punishEntry.setIpAddress(null);
                    BanSystem.getInstance().getPunishEntryRepository().save(punishEntry);
                    MessageType.SUCCESS_COMMAND_REMOVEIP.sendTo(sender, punishEntry.getPunishId(), punishEntry.getUser().getCachedName());
                    return;
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_REMOVEIP.sendTo(sender);
    }
}
