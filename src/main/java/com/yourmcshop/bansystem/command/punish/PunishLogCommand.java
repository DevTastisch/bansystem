package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishEntry;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishFormat;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.Comparator;
import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class PunishLogCommand extends Command {


    public PunishLogCommand() {
        super("punishlog");
    }

    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission("bansystem.punishlog")) {
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser != null) {
                    int maxLength = String.valueOf(
                            BanSystem.getInstance()
                                    .getPunishController()
                                    .getFormats()
                                    .stream()
                                    .max(Comparator.comparingInt(o -> String.valueOf(o.getId()).length()))
                                    .get()
                                    .getId())
                            .length();
                    for (PunishFormat format : BanSystem.getInstance().getPunishController().getFormats()) {
                        sender.sendMessage(MessageType.FORMAT_CHECK_PUNISHLOG.format(
                                format.getId(),
                                punishUser.getPunishEntries(format).size(),
                                String.format("%" + maxLength + "s", format.getName())
                        ));
                    }
                    return;
                }
                MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(sender, args[0]);
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        } else if (args.length == 2) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser != null) {
                    int id = Integer.valueOf(args[1]);
                    PunishFormat punishFormat = BanSystem.getInstance().getPunishController().getFormatById(id);
                    if (punishFormat != null) {
                        for (PunishEntry punishEntry : punishUser.getPunishEntries(punishFormat)) {
                            if (punishEntry.getPunishType() == PunishType.BAN) {
                                MessageType.INFO_COMMAND_CHECK_BAN_INFO.sendTo(sender,
                                        punishUser,
                                        punishEntry,
                                        punishEntry.getInvoker() == null ? MessageType.FORMAT_CONSOLE_BAN.format() : punishEntry.getInvoker().getCachedName(),
                                        punishEntry.getReasonId() == -1 ? punishEntry.getCustomReason() : punishEntry.getReason().getName(),
                                        punishEntry.getReasonId() == -1 ? "-" : punishEntry.getReason().getId()
                                );
                            } else {
                                MessageType.INFO_COMMAND_CHECK_MUTE_INFO.sendTo(sender,
                                        punishUser,
                                        punishEntry,
                                        punishEntry.getInvoker() == null ? MessageType.FORMAT_CONSOLE_BAN.format() : punishEntry.getInvoker().getCachedName(),
                                        punishEntry.getReasonId() == -1 ? punishEntry.getCustomReason() : punishEntry.getReason().getName(),
                                        punishEntry.getReasonId() == -1 ? "-" : punishEntry.getReason().getId()
                                );
                            }
                        }
                        return;
                    }
                    MessageType.ERROR_INVALID_ID.sendTo(sender, args[0]);
                    return;
                }
                MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(sender, args[0]);
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_PUNISHLOG.sendTo(sender);
    }
}
