package com.yourmcshop.bansystem.command.punish;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 04.03.2019
 */
public class ClearPunishHistory extends Command {


    public ClearPunishHistory() {
        super("clearpunishhistory");
    }

    public void execute(CommandSender sender, String[] args) {
        if(!sender.hasPermission("bansystem.clearpunishhistory")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(sender);
            return;
        }
        if (args.length == 1) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(sender, args[0]);
                    return;
                }

                BanSystem.getInstance().getPunishEntryRepository().findByUUID(punishUser.getUuid(), true)
                        .get()
                        .forEach(punishEntry -> BanSystem.getInstance().getPunishEntryRepository().delete(punishEntry));
                MessageType.SUCCESS_COMMAND_CLEARPUNISHHISTORY.sendTo(sender, punishUser);
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        MessageType.COMMAND_SYNTAX_CLEARPUNISHHISTORY.sendTo(sender);
    }
}
