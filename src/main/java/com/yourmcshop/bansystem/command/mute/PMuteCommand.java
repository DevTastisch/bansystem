package com.yourmcshop.bansystem.command.mute;

import com.yourmcshop.bansystem.BanSystem;
import com.yourmcshop.bansystem.data.PunishUser;
import com.yourmcshop.bansystem.message.MessageType;
import com.yourmcshop.bansystem.model.PunishType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.concurrent.ExecutionException;

/**
 * Created by DevTastisch on 17.03.2019
 */
public class PMuteCommand extends Command {

    public PMuteCommand() {
        super("pmute");
    }

    public void execute(CommandSender commandSender, String[] args) {
        if(!commandSender.hasPermission("bansystem.pmute")){
            MessageType.ERROR_NO_PERMISSIONS.sendTo(commandSender);
            return;
        }
        if (args.length == 2) {
            try {
                PunishUser punishUser = BanSystem.getInstance().getPunishUserRepository().findByCachedNameIgnoreCase(args[0]).get();
                if (punishUser == null) {
                    MessageType.ERROR_NOT_FOUND_PLAYER.sendTo(commandSender, args[0]);
                    return;
                }

                ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(punishUser.getUuid());
                if (BanSystem.getInstance().getPunishController().punish(commandSender, punishUser.getUuid(), args[1], PunishType.MUTE, -1, proxiedPlayer == null ? punishUser.getLastIp() : proxiedPlayer.getPendingConnection().getAddress().getHostName())) {
                    punishUser = BanSystem.getInstance().getPunishUserRepository().findByUniqueID(punishUser.getUuid()).get();
                    MessageType.SUCCESS_COMMAND_MUTE.sendTo(commandSender, punishUser, punishUser.getActiveMute());
                } else {
                    MessageType.ERROR_PLAYER_ALREADY_MUTED.sendTo(commandSender, punishUser);
                }
                return;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        MessageType.COMMAND_SYNTAX_PMUTE.sendTo(commandSender);
    }

}