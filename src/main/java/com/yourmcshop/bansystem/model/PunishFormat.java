package com.yourmcshop.bansystem.model;

import java.util.Collection;

/**
 * Created by DevTastisch on 01.03.2019
 */
public class PunishFormat {

    private final int id;
    private final String name;
    private final String displayType;
    private final String permission;
    private final Collection<PunishTime> punishTimes;

    public PunishFormat(int id, String name, String displayType, String permission, Collection<PunishTime> punishTimes) {
        this.id = id;
        this.name = name;
        this.displayType = displayType;
        this.permission = permission;
        this.punishTimes = punishTimes;
    }

    /**
     * @return template id
     */
    public int getId() {
        return id;
    }

    /**
     * @return template name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the shown displaytype as defined in banreasons.yml.
     */
    public String getDisplayType() {
        return displayType;
    }

    /**
     * @return the permission to use this template.
     */
    public String getPermission() {
        return permission;
    }

    public Collection<PunishTime> getPunishTimes() {
        return punishTimes;
    }


}
