package com.yourmcshop.bansystem.model;

import com.yourmcshop.bansystem.message.MessageType;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime implements Serializable {

    private final static SimpleDateFormat dateFormat;
    private final long timestamp;

    static {
        dateFormat = new SimpleDateFormat(MessageType.FORMAT_DATE_TIME_LAYOUT.format());
    }

    public DateTime(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the timestamp rounded to days.
     */
    public long getDays() {
        return (this.getSeconds() / (60 * 60 * 24));
    }

    /**
     * @return the timestamp rounded to hours.
     */
    public long getHours() {
        return (this.getSeconds() / (60 * 60));
    }

    /**
     * @return the timestamp rounded to minutes.
     */
    public long getMinutes() {
        return (this.getSeconds() / (60));
    }

    /**
     * @return the timestamp rounded to seconds.
     */
    public long getSeconds() {
        return (int) (this.timestamp / 1000);
    }

    /**
     * @return the amount of hours since 00:00 of the current day.
     */
    public int getHoursOfDay() {
        long dif = this.getSeconds();
        int days = (int) (dif / (60 * 60 * 24));
        dif -= days * 60 * 60 * 24;
        return (int) (dif / (60 * 60));
    }

    /**
     * @return the amount of minutes since last full hour.
     */
    public int getMinutesOfHour() {
        long dif = this.getSeconds();
        int days = (int) (dif / (60 * 60 * 24));
        dif -= days * 60 * 60 * 24;
        int hours = (int) (dif / (60 * 60));
        dif -= hours * 60 * 60;
        return (int) (dif / 60);
    }

    /**
     * @return the amount of seconds since last full minute.
     */
    public int getSecondsOfMinute() {
        long dif = this.getSeconds();
        int days = (int) (dif / (60 * 60 * 24));
        dif -= days * 60 * 60 * 24;
        int hours = (int) (dif / (60 * 60));
        dif -= hours * 60 * 60;
        int minutes = (int) (dif / 60);
        dif -= minutes * 60;
        return (int) (dif);

    }

    /**
     * @return the timestamp as milliseconds.
     */
    public long getMillis() {
        return this.timestamp;
    }

    /**
     * @return formatted after config entry display.datetimelayout.
     */
    public String formatAsDateTime() {
        if(this.getMillis() == -1) return "-";
        return dateFormat.format(new Date(this.timestamp));
    }

    /**
     * @return formattet after config entry display.timelayout.
     */
    public String formatAsTimeUnits() {
        if(this.getMillis() == -1 ) return "Permanent";
        return MessageType.FORMAT_TIME_LAYOUT.format(
                this.getDays() == 0 ? "" :
                        MessageType.FORMAT_TIME_LAYOUT_DAYS.format(this.getDays()),
                this.getHoursOfDay() == 0 ? "" :
                        MessageType.FORMAT_TIME_LAYOUT_HOURS.format(this.getHoursOfDay()),
                this.getMinutesOfHour() == 0 ? "" :
                        MessageType.FORMAT_TIME_LAYOUT_MINUTES.format(this.getMinutesOfHour()),
                this.getSecondsOfMinute() == 0 ? "" :
                        MessageType.FORMAT_TIME_LAYOUT_SECONDS.format(this.getSecondsOfMinute())
        );
    }

}
